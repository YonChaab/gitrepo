package org.sid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class AppSpringBootApplication implements CommandLineRunner {

	@Autowired
	private EtudiantRepository etudiantRepository;

	@Autowired
	private FormationRepository formationRepository;
	
	@Autowired
	private RepositoryRestConfiguration repositoryRestConfiguration;

	public static void main(String[] args) {
		SpringApplication.run(AppSpringBootApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		repositoryRestConfiguration.exposeIdsFor(Formation.class,Etudiant.class);
		
		Formation f1=formationRepository.save(new Formation(null, "Java", 30, null));
		Formation f2=formationRepository.save(new Formation(null, "Spring", 60, null));
		Formation f3=formationRepository.save(new Formation(null, "Angular", 20, null));

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		etudiantRepository.save(new Etudiant(null, "Chaab", "yosra", df.parse("08/04/1990"),f1));
		etudiantRepository.save(new Etudiant(null, "Chaab", "yon", df.parse("08/04/1990"),f2));
		etudiantRepository.save(new Etudiant(null, "Chaab", "yassou", df.parse("08/04/1990"),f3));

	}

}
