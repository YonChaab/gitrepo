package mypackage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import mypackage.dao.TaskRepository;
import mypackage.entities.Task;

@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private TaskRepository taskRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		taskRepository.save(new Task("task1","desc1",df.parse("2018-10-12 12:02:02"),false));
		taskRepository.save(new Task("task2","desc2",df.parse("2018-10-10 10:02:02"),true));
		taskRepository.save(new Task("task3","desc3",df.parse("2018-10-11 11:02:02"),false));
		taskRepository.save(new Task("task4","desc4",df.parse("2018-10-09 09:02:02"),true));
		
		taskRepository.findAll().forEach(c->{
			System.out.println(c.getName());
		});
		

	}

}
