package mypackage.dao;

import org.springframework.data.repository.CrudRepository;

import mypackage.entities.Task;

public interface TaskRepository extends CrudRepository<Task, Integer>{

}
