package org.sid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sid.entities.Employee;
import org.sid.entities.EmployeeRepository;
import org.sid.entities.User;
import org.sid.entities.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class WemanityAppApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private RepositoryRestConfiguration repositoryRestConfiguration;

	public static void main(String[] args) {
		SpringApplication.run(WemanityAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		repositoryRestConfiguration.exposeIdsFor(User.class,Employee.class);
		
		User u1 = userRepository.save(new User(null, "yosra", "chaabane", true, new Date(), new Date()));
		User u2 = userRepository.save(new User(null, "yon", "chaabane", true, new Date(), new Date()));
		User u3 = userRepository.save(new User(null, "achref", "achref", true, new Date(), new Date()));

		employeeRepository
				.save(new Employee(null, "achref", "bz", "22145654", "masculin", "moknin", "mestir", "tunisia", u3));

	}

}
